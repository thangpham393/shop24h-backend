import express from 'express';
import mongoose from 'mongoose'

import productRoutes from './server/routes/product';
import customerRoutes from './server/routes/customer';
import orderRoutes from './server/routes/order';
import orderDetailRoutes from './server/routes/orderDetail';

//Get ENV Variables
import dotenv from 'dotenv';

dotenv.config();

// set up dependencies
const app = express();

// get JSON POST request body 
app.use(express.urlencoded({
    extended: true
  }));
  app.use(express.json());
  
  // set up mongoose
  mongoose.connect(process.env.MONGODB_URL, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(()=> {
      console.log('Database connected');
    })
    .catch((error)=> {
      console.log('Error connecting to database');
    });

// get JSON POST request body 
app.use(express.urlencoded({
    extended: true
  }));
  app.use(express.json());

  // set up port
const port = 8000;

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// set up route
app.get('/', (req, res) => {
    res.status(200).json({
      message: 'Welcome to Project with Nodejs Express and MongoDB',
    });
  });
  // set up route
  app.use('/', productRoutes);
  app.use('/', customerRoutes);
  app.use('/', orderRoutes)
  app.use('/', orderDetailRoutes )

app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
  });