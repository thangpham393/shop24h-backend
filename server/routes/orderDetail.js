import express from 'express';
import { createOrderDetail, deleteOrderDetail, getAllOrderDetail, getOneOrderDetail,updateOrderDetail } from '../controllers/orderDetail';


const router = express.Router()

router.post('/orderdetails', createOrderDetail);
router.get('/orderdetails', getAllOrderDetail)
router.get('/orderdetails/:orderDetailId', getOneOrderDetail);
router.put('/orderdetails/:orderDetailId', updateOrderDetail);
router.delete('/orderdetails/:orderDetailId', deleteOrderDetail);
export default router