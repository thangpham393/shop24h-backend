import express from 'express';
import { createCustomer, deleteCustomer, getAllCustomer, getCustomerByEmail, getOneCustomer, updateCustomer } from '../controllers/customer';


const router = express.Router()

router.post('/customers', createCustomer);
router.get('/customers', getAllCustomer);
router.get('/customers/:customerId', getOneCustomer);
router.get('/customers/by-email/:email', getCustomerByEmail);
router.put('/customers/:customerId', updateCustomer);
router.delete('/customers/:customerId', deleteCustomer);

export default router