import express from 'express';
import { createProduct, deleteProduct, getAllProduct, getProductById, updateProduct } from '../controllers/product';


const router = express.Router()

router.post('/products', createProduct);
router.get('/products', getAllProduct);
router.get('/products/:productId', getProductById);
router.put('/products/:productId', updateProduct);
router.delete('/products/:productId', deleteProduct);

export default router