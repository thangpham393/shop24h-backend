import express from 'express';
import { createNewOrder, deleteOrder, getAllOrder, getAllOrderOfCustomer, getOneOrder, updateOrder } from '../controllers/Order';



const router = express.Router()
router.get('/orders',getAllOrder);
router.get('/orders/:orderId',getOneOrder);
router.post('/customers/:customerId/orders', createNewOrder);
router.get('/customers/:customerId/orders', getAllOrderOfCustomer);
router.put('/orders/:orderId', updateOrder);
router.delete('/orders/:orderId', deleteOrder);


export default router