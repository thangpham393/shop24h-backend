import mongoose from 'mongoose';
import Customer from '../models/customer';

// create new customer
export function createCustomer(req, res) {
    const customer = new Customer({
        _id: mongoose.Types.ObjectId(),
        FullName: req.body.FullName,
        PhoneNumber: req.body.PhoneNumber,
        Email: req.body.Email,
        Address: req.body.Address,
        City: req.body.City,
        Country: req.body.Country,
        TimeCreated: new Date(),
        TimeUpdated: new Date(),
    });

    return customer.save()
        .then((newCustomer) => {
            return res.status(200).json({
                success: true,
                message: 'New Customer created successfully',
                Customer: newCustomer,
            });
        })
        .catch((error) => {
            console.log(error);
            res.status(500).json({
                success: false,
                message: 'Server error. Please try again.',
                error: error.message,
            });
        });
}

// get all Customer 
export function getAllCustomer(req, res) {
    let condition = {};
    if(req.query.email){
        condition = {'Email' : req.query.email}
    }
    Customer.find(condition)
        .select('_id FullName PhoneNumber Email Address City Country')
        .then((allCustomer) => {
            return res.status(200).json({
                success: true,
                message: 'A list of all customers',
                Customer: allCustomer,
            });
        })
        .catch((err) => {
            res.status(500).json({
                success: false,
                message: 'Server error. Please try again.',
                error: err.message,
            });
        });
}

// Get one Customer
export function getOneCustomer(req, res) {
    const id = req.params.customerId;
    Customer.findById(id)
        .then((singleCustomer) => {
            res.status(200).json({
                success: true,
                message: `More on ${singleCustomer.FullName}`,
                Customer: singleCustomer,
            });
        })
        .catch((err) => {
            res.status(500).json({
                success: false,
                message: 'This Customer does not exist',
                error: err.message,
            });
        });
} 
// Get one Customer by Email
export function getCustomerByEmail(req,res){
       let condition = {'Email' : req.params.email}
    
    Customer.findOne(condition)
    .then((singleCustomer) => {
        res.status(200).json({
            success: true,
            message: `More on ${singleCustomer.FullName}`,
            Customer: singleCustomer,
        });
    })
    .catch((err) => {
        res.status(500).json({
            success: false,
            message: 'This Customer does not exist',
            error: err.message,
        });
    });
}

// update Customer
export function updateCustomer(req, res) {
    const id = req.params.customerId;
    const updateObject = req.body;
    Customer.update({
            _id: id
        }, {
            $set: updateObject
        })
        .exec()
        .then(() => {
            res.status(200).json({
                success: true,
                message: 'Customer is updated',
                updateCustomer: updateObject,
            });
        })
        .catch((err) => {
            res.status(500).json({
                success: false,
                message: 'Server error. Please try again.'
            });
        });
}

// delete a Customer
export function deleteCustomer(req, res) {
    const id = req.params.customerId;
    Customer.findByIdAndRemove(id)
        .exec()
        .then(() => res.status(200).json({
            success: true,
        }))
        .catch((err) => res.status(500).json({
            success: false,
        }));
}