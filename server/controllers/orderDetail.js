import mongoose from 'mongoose';
import OrderDetail from '../models/orderDetail';

// create new orderdetail
export function createOrderDetail(req, res) {
    const orderDetail = new OrderDetail({
        _id: mongoose.Types.ObjectId(),
        Order : req.body.Order,
        Product: req.body.Product,
        Quantity: req.body.Quantity,
        PriceEach: req.body.PriceEach,
    });

    return orderDetail.save()
        .then((newOrderDetail) => {
            return res.status(200).json({
                success: true,
                message: 'New OrderDetail created successfully',
                OrderDetail: newOrderDetail,
            });
        })
        .catch((error) => {
            console.log(error);
            res.status(500).json({
                success: false,
                message: 'Server error. Please try again.',
                error: error.message,
            });
        });
} 

// get ALL ORDERDETAIL
export function getAllOrderDetail(req, res) {
    OrderDetail.find()
        .select('_id Order Product Quantity PriceEach')
        .then((allOrderDetail) => {
            return res.status(200).json({
                success: true,
                message: 'A list of all OrderDetail',
                OrderDetail: allOrderDetail,
            });
        })
        .catch((err) => {
            res.status(500).json({
                success: false,
                message: 'Server error. Please try again.',
                error: err.message,
            });
        });
}

// Get one OrderDetail
export function getOneOrderDetail(req, res) {
    const id = req.params.orderDetailId;
    OrderDetail.findById(id)
        .then((singleOrderDetail) => {
            res.status(200).json({
                success: true,
                message: `More on ${singleOrderDetail.Order}`,
                OrderDetail: singleOrderDetail,
            });
        })
        .catch((err) => {
            res.status(500).json({
                success: false,
                message: 'This OrderDetail does not exist',
                error: err.message,
            });
        });
} 

// update OrderDetail
export function updateOrderDetail(req, res) {
    const id = req.params.orderDetailId;
    const updateObject = req.body;
    OrderDetail.update({
            _id: id
        }, {
            $set: updateObject
        })
        .exec()
        .then(() => {
            res.status(200).json({
                success: true,
                message: 'OrderDetail is updated',
                updateOrderDetail: updateObject, 
            });
        })
        .catch((err) => {
            res.status(500).json({
                success: false,
                message: 'Server error. Please try again.'
            });
        });
} 


// delete a OrderDetail
export function deleteOrderDetail(req, res) {
    const id = req.params.orderDetailId;
    OrderDetail.findByIdAndRemove(id)
        .exec()
        .then(() => res.status(200).json({
            success: true,
        }))
        .catch((err) => res.status(500).json({
            success: false,
        }));
}
