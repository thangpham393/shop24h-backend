import mongoose from 'mongoose';
import Customer from '../models/customer';
import Order from '../models/order';

// create new Order
export function createNewOrder(req, res) {
    const order = new Order({
        _id: mongoose.Types.ObjectId(),
        OrderDate: new Date(),
        RequiredDate: new Date(),
        ShippedDate: new Date(),
        Note: req.body.Note,
        Status: req.body.Status,
        TimeCreated: new Date(),
        TimeUpdated: new Date(),
    });

    return order.save()
        .then(function(newOrder){
            var customerId = req.params.customerId;
            return Customer.findOneAndUpdate({_id: customerId} , {$push:{Orders: newOrder._id}}, {new: true});
        })
        .then((updateCustomer) => {
            return res.status(200).json({
                success: true,
                message: 'New Order created successfully on Customer',
                Order: order,
                Customer: updateCustomer,
            });
        })
        .catch((error) => {
            console.log(error);
            res.status(500).json({
                success: false,
                message: 'Server error. Please try again.',
                error: error.message,
            });
        });
}

// get ALL ORDER 
export function getAllOrder(req, res) {
    Order.find()
        .select('_id OrderDate ShippedDate Note Status')
        .then((allOrder) => {
            return res.status(200).json({
                success: true,
                message: 'A list of all Orders',
                Order: allOrder,
            });
        })
        .catch((err) => {
            res.status(500).json({
                success: false,
                message: 'Server error. Please try again.',
                error: err.message,
            });
        });
}

// get All Order Of Customer
export function getAllOrderOfCustomer(req, res) {
    const id = req.params.customerId;
    Customer.findById(id)
        .populate({path:'Orders'})
        .then((singleCustomer) => {
            res.status(200).json({
                success: true,
                message: `All orders of customer ${singleCustomer.FullName}`,
                Orders: singleCustomer.Orders,
            });
        })
        .catch((err) => {
            res.status(500).json({
                success: false,
                message: 'This Order does not exist',
                error: err.message,
            });
        });
} 

// Get One Order
export function getOneOrder(req, res) {
    const id = req.params.orderId;
    Order.findById(id)
        .then((singleOrder) => {
            res.status(200).json({
                success: true,
                message: `More on ${singleOrder._id}`,
                Order: singleOrder,
            });
        })
        .catch((err) => {
            res.status(500).json({
                success: false,
                message: 'This Order does not exist',
                error: err.message,
            });
        });
} 
// update Order
export function updateOrder(req, res) {
    const id = req.params.orderId;
    const updateObject = req.body;
    Order.update({
            _id: id
        }, {
            $set: updateObject
        })
        .exec()
        .then(() => {
            res.status(200).json({
                success: true,
                message: 'Order is updated',
                updateOrder: updateObject,
            });
        })
        .catch((err) => {
            res.status(500).json({
                success: false,
                message: 'Server error. Please try again.'
            });
        });
}

// delete a Order
export function deleteOrder(req, res) {
    const id = req.params.orderId;
    Order.findByIdAndRemove(id)
        .exec()
        .then(() => res.status(200).json({
            success: true,
        }))
        .catch((err) => res.status(500).json({
            success: false,
        }));
}