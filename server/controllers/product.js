import mongoose from 'mongoose';
import Product from '../models/product';

// create new product
export function createProduct(req, res) {
    const product = new Product({
        _id: mongoose.Types.ObjectId(),
        product: req.body.product,
        Name: req.body.Name,
        Type: req.body.Type,
        ImageUrl: req.body.ImageUrl,
        BuyPrice: req.body.BuyPrice,
        PromotionPrice: req.body.PromotionPrice,
        Description: req.body.Description,
        TimeCreated: new Date(),
        TimeUpdated: new Date(),
    });

    return product.save()
        .then((newProduct) => {
            return res.status(200).json({
                success: true,
                message: 'New Product created successfully',
                Product: newProduct,
            });
        })
        .catch((error) => {
            console.log(error);
            res.status(500).json({
                success: false,
                message: 'Server error. Please try again.',
                error: error.message,
            });
        });
}

// get ALLPRODUCT 
export function getAllProduct(req, res) {
    Product.find()
        .select('_id Name Type ImageUrl BuyPrice PromotionPrice Description')
        .then((allProduct) => {
            return res.status(200).json({
                success: true,
                message: 'A list of all products',
                Product: allProduct,
            });
        })
        .catch((err) => {
            res.status(500).json({
                success: false,
                message: 'Server error. Please try again.',
                error: err.message,
            });
        });
}

// Get Product by ID
export function getProductById(req, res) {
    const id = req.params.productId;
    Product.findById(id)
        .then((singleProduct) => {
            res.status(200).json({
                success: true,
                message: `More on ${singleProduct.Name}`,
                Product: singleProduct,
            });
        })
        .catch((err) => {
            res.status(500).json({
                success: false,
                message: 'This Product does not exist',
                error: err.message,
            });
        });
} 

// update Product
export function updateProduct(req, res) {
    const id = req.params.productId;
    const updateObject = req.body;
    Product.update({
            _id: id
        }, {
            $set: updateObject
        })
        .exec()
        .then(() => {
            res.status(200).json({
                success: true,
                message: 'Product is updated',
                updateProduct: updateObject,
            });
        })
        .catch((err) => {
            res.status(500).json({
                success: false,
                message: 'Server error. Please try again.'
            });
        });
}

// delete a Product
export function deleteProduct(req, res) {
    const id = req.params.productId;
    Product.findByIdAndRemove(id)
        .exec()
        .then(() => res.status(200).json({
            success: true,
        }))
        .catch((err) => res.status(500).json({
            success: false,
        }));
}