import mongoose from 'mongoose';

mongoose.Promise = global.Promise;

const orderDetailSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    Order : {
        type:mongoose.Schema.Types.ObjectId,
        ref:"Order",
        required: true
    },
	Product: {
        type:mongoose.Schema.Types.ObjectId,
        ref:"Product",
        required: true
    },
	Quantity: Number,
	PriceEach: Number

})

export default mongoose.model('OrderDetail', orderDetailSchema)