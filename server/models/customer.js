import mongoose from 'mongoose';

mongoose.Promise = global.Promise;

const customerSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    FullName: String,
    PhoneNumber: String,
    Email: {
        type: String,
        unique: true
    },
    Address: String,
    City: String,
    Country: String,
    TimeCreated: { type: Date, default: Date.now },
    TimeUpdated: { type: Date, default: Date.now },
    Orders : [{
        type:mongoose.Schema.Types.ObjectId,
        ref:"Order"
    }],
})

export default mongoose.model('Customers', customerSchema)