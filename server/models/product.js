import mongoose from 'mongoose';

mongoose.Promise = global.Promise;

const productSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    Name: String,
    Type: String,
    ImageUrl: String,
    BuyPrice: Number,
    PromotionPrice: Number,
    Description: String,
    TimeCreated: { type: Date, default: Date.now },
    TimeUpdated: { type: Date, default: Date.now }

})

export default mongoose.model('Product', productSchema)