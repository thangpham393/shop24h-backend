import mongoose from 'mongoose';

mongoose.Promise = global.Promise;

const orderSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    OrderDate: { type: Date, default: Date.now },
    RequiredDate: { type: Date, default: Date.now },
    ShippedDate: { type: Date, default: Date.now },
    Note: String,
    Status:{ type:Number, default: 0 },
    TimeCreated:  { type: Date, default: Date.now },	
    TimeUpdated:  { type: Date, default: Date.now },
})

export default mongoose.model('Order', orderSchema)